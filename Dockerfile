FROM node

WORKDIR /app

COPY package.json /app
COPY yarn.lock /app

RUN yarn install

COPY . /app

RUN yarn

ENTRYPOINT ["/app/cli.js"]
